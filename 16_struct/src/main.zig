const std = @import("std");

const Point = @import("Point.zig");

// Define a simple struct.
// const Point = struct {
//     x: f32,
//     y: f32 = 0,
//
//     // Namespaced function.
//     fn new(x: f32, y: f32) Point {
//         // Anonymous struct literal.
//         return .{ .x = x, .y = y };
//     }
//
//     // Method.
//     fn distance(self: Point, other: Point) f32 {
//         const diffx = other.x - self.x;
//         const diffy = other.y - self.y;
//         return @sqrt(diffx * diffx + diffy * diffy);
//     }
// };

// A struct can have declarations and no fields at all. If it has no
// fields, it's basically a just a namespace.
const Namespace = struct {
    const pi: f64 = 3.141592;
    var count: usize = 0;
};

pub fn main() !void {
    // Anonymous struct literal. y omitted since it has default value.
    const a_point: Point = .{ .x = 0 };
    // Using namespaced function.
    const b_point = Point.new(1, 1);

    // Using method syntax.
    std.debug.print("distance: {d:.1}\n", .{a_point.distance(b_point)});
    // Using namespaced function syntax.
    std.debug.print("distance: {d:.1}\n", .{Point.distance(a_point, b_point)});

    std.debug.print("\n", .{});

    // A namespace struct with no fields has size 0.
    std.debug.print("size of Point: {}\n", .{@sizeOf(Point)});
    std.debug.print("size of Namespace: {}\n", .{@sizeOf(Namespace)});

    std.debug.print("\n", .{});

    // @fieldParentPtr demo.
    var c_point = Point.new(0, 0);
    setYBasedOnX(&c_point.x, 42);
    std.debug.print("c_point.y: {d:.1}\n", .{c_point.y});

    std.debug.print("\n", .{});

    // The dot operator de-references a pointer to a struct automatically
    // when accessing fields or calling methods.
    const c_point_ptr = &c_point;
    std.debug.print("c_point_ptr.y: {d:.1}, c_point_ptr.*.y: {d:.1}\n", .{ c_point_ptr.y, c_point_ptr.*.y });
}

// struct field order is determined by the compiler for optimal performance.
// however, you can still calculate a struct base pointer given a field pointer:
fn setYBasedOnX(x: *f32, y: f32) void {
    // @fieldParentPtr lets you get a field's parent from a pointer to that field.
    const point = @fieldParentPtr(Point, "x", x);
    point.y = y;
}
