const std = @import("std");

fn add(a: u8, b: u16) u32 {
    return a + b;
}

pub fn main() !void {
    // Coercions only happen when it's safe to do so.
    // Int widening.
    const a: u8 = 42; // Also demonstrates comptime_int coercion.
    const b: u16 = a;
    const c: u32 = b;
    const d: u64 = c;
    const e: u128 = d;
    std.debug.print("int widening: u8 ...-> u128: {} {}\n\n", .{ e, @TypeOf(e) });

    // Float widening.
    const f: f16 = 3.1415; //  Also demonstrates comptime_float coercion.
    const g: f32 = f;
    const h: f64 = g;
    const i: f80 = h;
    const j: f128 = i;
    std.debug.print("float widening: f16 ...-> f128: {} {}\n\n", .{ j, @TypeOf(j) });

    // Function call args coercion.
    const a_u8: u8 = 42;
    const a_result = add(a_u8, a_u8);
    std.debug.print("function args: u8 -> u16 -> u32: {} {}\n\n", .{ a_result, @TypeOf(a_result) });

    // Safe narrowing of comptime known values.
    const b_u64: u64 = 2000;
    const b_result = add(a_u8, b_u64);
    std.debug.print("comptime narrowing u64 -> u32? {} {}\n\n", .{ b_result, @TypeOf(b_result) });

    // Comptime known unsafe narrowing causes compile error.
    // const c_u64: u64 = 20_000_000_000;
    // const d_u32: u32 = c_u64;
    // _ = d_u32;

    // Coercion can happen between pointer types.
    const array = [_]u8{ 1, 2, 3 };
    const b_slice: []const u8 = &array;
    const c_mptr: [*]const u8 = &array;
    _ = c_mptr;

    // Coercion can happen between child type and optional.
    var a_optional: ?u8 = null;
    a_optional = a_u8;
    // Same with error unions.
    var a_err: anyerror!u8 = error.InvalidNumber;
    a_err = a_u8;

    // Peer type resolution requires all branches to coerce to a common type.
    var runtime_zero: usize = 0;
    var happy_medium = if (runtime_zero > 12) a else f;
    std.debug.print("peer if: u8 or f16 -> f16: {} {}\n\n", .{ happy_medium, @TypeOf(happy_medium) });

    // Also happens in some binary operations.
    const c_result = a + e;
    std.debug.print("peer add: u8 + u128 -> u128: {} {}\n\n", .{ c_result, @TypeOf(c_result) });

    // When type coercion can't be done safely, you must explicitly cast via
    // the many casting builtins.
    const k: u8 = @intFromFloat(f);
    std.debug.print("@intFromFloat: f16 -> u8: {} {}\n\n", .{ k, @TypeOf(k) });

    const l: f16 = @floatFromInt(c);
    std.debug.print("@floatFromInt: u32 -> f16: {} {}\n\n", .{ l, @TypeOf(k) });

    // You can cast pointers too.
    const d_mptr: [*]const u8 = @ptrCast(b_slice);
    std.debug.print("@ptrCast: []const u8 -> [*]const u8: {}\n\n", .{@TypeOf(d_mptr)});

    // You can even re-interpret the bits of a value of one type
    // as another type without changing those bits.
    const float: f64 = 3.141592;
    const bits: u64 = @bitCast(float);
    const as_u64: u64 = @intFromFloat(float);
    const as_f64: f64 = @bitCast(bits);
    std.debug.print("@bitCast:\n", .{});
    std.debug.print("f64: {d}\n", .{float});
    std.debug.print("u64 bits: {b}\n", .{bits});
    std.debug.print("u64 re-interpret: {d}\n", .{as_u64});
    std.debug.print("u64 re-interpret bits: {b}\n", .{as_u64});
    std.debug.print("back to f64: {d}\n\n", .{as_f64});

    // Narrowing int cast. If acceptable you can truncate bits.
    var big_int: u64 = 20_000_000_000;
    var little_int: u32 = @truncate(big_int);
    std.debug.print("@intCast narrowing: u64 -> u32: {}\n\n", .{little_int});
}
